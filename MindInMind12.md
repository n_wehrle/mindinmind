# We Have Time Requirements
## Responsiveness
### Context
* Is not the same as performance!
* Not just an implementation issue, but a design issue.
* Not solvable only by performance tuning or hardware updates.

### How to get it
* immediate feedback (0.1s)
* how long will it take
* cancel button

## Durations
* 0.001s detectable audio gap
* ~~0.01s shortest visual perception~~
* 0.1s one moment
  * feedback hand-eye coordination
  * feedback button click
  * busy indicator
  * maximum interval animation frames vs 24fps
* 1s conversation gap
  * progressbar for long running operations
  * finishing user requests (open window)
  * finishing unrequested ops (auto save)
  * wait time after important info 
* 10s unbroken attention to task
  * completing user input
  * completing wizard page
  * heavyweight tasks like file transfers
* 100s critical descision in emergency situation
  * assure all information is there
  
## Additional guidelines
### Busy indicators
* static and animated on mouse cursor
* for all blocking ops

### Progress indicators
* how much time remains
* op that is longer than a few seconds
* show work remaining
* show total progress
* start percentage with 1%
* show 100% very briefly
* smooth animation
* human scale precision

### Delays
* between completed hight level tasks may not affect performance
* within a group of lowlevel tasks =very sensitive (forget planned steps)

### Display (Rendering)
* Order: important .. details .. auxiliary
* progressive rendering (progressive images)
* Example Word: show first page quick
* Example Search results: show when available
* Fake heavyweight computations during hand-eye coordination
* Example: rubberband
* Example: probability (wow)
* Example: estimations (word processors)

### Work ahead
* render next page or picture
* probability: what will the user do next?
* Example: shop item, mouse comes near, load detail image
* Reorder task by priority (Airport, Abort page load)
* Decrease detail to keep animations smooth (framerate)

### Timely feedback on the Web
* Design: use thumbnails
* Design: drilldowns (level of detail)
* Technical: own topic
* Book: High performance Websites

