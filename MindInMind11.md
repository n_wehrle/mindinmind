# Many Factors affect learning
How can we design a GUI so that using it becomes automatic?

* Operation is task-focused, simple, and consistent
* Vocabulary is task-focused, familiar, and consistent
* Risk is low

## Operation
**Gulf of execution**
is the gap between what the user wants and the tool provides.

_Picture with telescope_

### Task analysis
* What goals do users want to achieve by using the application?
* What set of human tasks is the application intended to support?
* Which tasks are common, and which ones are rare?
* Which tasks are most important, and which ones are least important?
* What are the steps of each task?
* What are the result and output of each task?
* Where does the information for each task come from?
* How is the information that results from each task used?
* Which people do which tasks?
* What tools are used to do each task?
* What problems do people have performing each task? What sorts of mistakes are common? What causes them? How damaging are mistakes?
* What terminology do people who do these tasks use?
* What communication with other people is required to do the tasks?
* How are different tasks related?

### Conceptual model
* conceptual objects
* actions
* object settings
* relations

> If it isn’t in the objects/actions analysis,
> users shouldn’t know about it.
> I don't care about your LinkedHashMap!
      
*Example product comments: customer, product, comment, response.*
*Not: database, table, view, cookie*
*Actions: view, comment, respond, edit*
*Attributes: title, creator, date*

#### Simplicity, Complexity, Consistancy
* Less is more, *Example: priorities in a todo app.*
* Resist adding extra functionality, it increases complexity. *Screenshot*
* Predictable = consistant = less to learn
* Create an object-action matrix
* many objects, but the same actions on every object (consistancy)

### User Interface Design
* keystroke consistancy (mouse movement and keystrokes) does not depend on context
* Internal Styleguides, User Interface guidelines …

## Vocabulary
* familiar terminology *Screenshot*
* consistent
> Same name, same thing; different name, different thing. 
> (Caroline Jarret)
* term overloading, don't do it
* use industry standard names for GUI concepts
* create a product lexicon

## Risk
low risk -> try new things

* analogy: cities
* feature overkill
* prevent errors
* deactivate invalid commands
* make errors easy to detect
* allow to correct errors easily




      
      
      
      


      
      


 