# Designing with the mind a mind
I read the book "Designing with the mind in mind" and had to summarize the chapters 11, 12 and 13 in a talk.
The book is a nice read for UX and UI Designers. These are the slides and my personal notes.

![cover](https://bitbucket.org/n_wehrle/mindinmind/raw/master/cover.jpg)

## Chapters
- Many Factors affect learning
- We Have Time Requirements
- Epilogue